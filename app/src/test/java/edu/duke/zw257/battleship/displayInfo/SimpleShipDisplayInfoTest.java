package edu.duke.zw257.battleship.displayInfo;

import edu.duke.zw257.battleship.displayInfo.SimpleShipDisplayInfo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleShipDisplayInfoTest {

    @Test
    void test_getInfo() {
        SimpleShipDisplayInfo<Character> ss = new SimpleShipDisplayInfo<>('s', 'x');
        assertEquals('x', ss.getInfo(null, true));
        assertEquals('s', ss.getInfo(null, false));

    }
}