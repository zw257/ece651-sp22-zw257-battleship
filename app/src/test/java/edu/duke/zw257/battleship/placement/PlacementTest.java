package edu.duke.zw257.battleship.placement;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlacementTest {

    @Test
    void testEquals_HashCode_toString() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'v');
        Placement p3 = new Placement(c1, 'A');
        Placement p4 = new Placement(c2, 'a');
        // Equals
        assertEquals(p1, p1); // equals should be reflexsive
        assertEquals(p1, p2); // different objects but same contents
        assertNotEquals(p1, p3); // different contents
        assertNotEquals(p1, p4);
        assertEquals(p3, p4);
        assertNotEquals(c1, "(1, 2)"); // different types
        // HashCode and toString
        assertEquals(p1.hashCode(), p1.hashCode()); // equals should be reflexsive
        assertEquals(p1.hashCode(), p2.hashCode()); // different objects but same contents
        assertNotEquals(p1.hashCode(), p3.hashCode()); // different contents
        assertNotEquals(p1.hashCode(), p4.hashCode());
        assertEquals(p3.hashCode(), p4.hashCode());
        assertNotEquals(c1.hashCode(), "(1, 2)"); // different types
    }

    @Test
    void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("B3v");
        assertEquals(1, p1.getCoordinate().getRow());
        assertEquals(3, p1.getCoordinate().getColumn());
        assertEquals('V', p1.getOrientation());
        Placement p2 = new Placement("D5h");
        assertEquals(3, p2.getCoordinate().getRow());
        assertEquals(5, p2.getCoordinate().getColumn());
        assertEquals('H', p2.getOrientation());
        Placement p3 = new Placement("A9H");
        assertEquals(0, p3.getCoordinate().getRow());
        assertEquals(9, p3.getCoordinate().getColumn());
        assertEquals('H', p3.getOrientation());
        Placement p4 = new Placement("Z0V");
        assertEquals(25, p4.getCoordinate().getRow());
        assertEquals(0, p4.getCoordinate().getColumn());
        assertEquals('V', p4.getOrientation());
    }

    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("A0--"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("00Z"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("0"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("00"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("000"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("0000"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("Z0-"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAAA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@02"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("[0D"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A/f"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("As:"));
        assertThrows(IllegalArgumentException.class, () -> new Placement(""));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A \0"));
    }
}