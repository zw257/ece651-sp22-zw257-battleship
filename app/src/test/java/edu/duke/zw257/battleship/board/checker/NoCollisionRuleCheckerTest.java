package edu.duke.zw257.battleship.board.checker;

import edu.duke.zw257.battleship.board.BattleShipBoard;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.V1ShipFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NoCollisionRuleCheckerTest {


    @Test
    void test_checkMyRule() {
        String noCollisionRuleCheckerFailMSG = "That placement is invalid: the ship overlaps another ship.\n";
        V1ShipFactory f = new V1ShipFactory();
        BattleShipBoard<Character> board = new BattleShipBoard<>(10, 20, new NoCollisionRuleChecker<>(new InBoundsRuleChecker<>(null)), 'X');
        assertNull(board.tryAddShip(f.makeBattleship(new Placement("A0H"))));
        assertNull(board.tryAddShip(f.makeBattleship(new Placement("A4H"))));
        assertEquals(noCollisionRuleCheckerFailMSG, board.tryAddShip(f.makeBattleship(new Placement("a7v"))));
        assertNull(board.tryAddShip(f.makeBattleship(new Placement("Q0v"))));
        assertEquals(noCollisionRuleCheckerFailMSG, board.tryAddShip(f.makeBattleship(new Placement("R0v"))));
    }

}