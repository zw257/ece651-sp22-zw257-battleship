package edu.duke.zw257.battleship.view;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.board.BattleShipBoard;
import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.ship.RectangleShip;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class BoardTextViewTest {

    @Test
    void displayMyOwnBoard() {
        Board<Character> b1 = new BattleShipBoard<>(2, 2, 'X');
        BoardTextView view = new BoardTextView(b1);
        String expected =
                "  0|1\n"+
                "A  |  A\n"+
                "B  |  B\n"+
                "  0|1\n";
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_2by2 () {
        String expectedHeader = "  0|1\n";
        String expectedOwnBody =
                "A  |  A\n" +
                "B  |  B\n";
        String expectedEnemyBody =
                "A  |  A\n" +
                "B  |  B\n";
        Board<Character> b1 = new BattleShipBoard<>(2, 2, 'X');
        boardCheckerHelper(b1, expectedHeader, expectedOwnBody, expectedEnemyBody);
    }

    @Test
    public void test_display_empty_3by2 () {
        String expectedHeader = "  0|1|2\n";
        String expectedOwnBody =
                "A  | |  A\n" +
                "B  | |  B\n";
        String expectedEnemyBody =
                "A  | |  A\n" +
                "B  | |  B\n";
        Board<Character> b1 = new BattleShipBoard<>(3, 2, 'X');
        boardCheckerHelper(b1, expectedHeader, expectedOwnBody, expectedEnemyBody);
    }

    @Test
    public void test_display_empty_3by5 () {
        String expectedHeader = "  0|1|2\n";
        String expectedOwnBody =
                "A  | |  A\n" +
                "B  | |  B\n" +
                "C  | |  C\n" +
                "D  | |  D\n" +
                "E  | |  E\n";
        String expectedEnemyBody =
                "A  | |  A\n" +
                "B  | |  B\n" +
                "C  | |  C\n" +
                "D  | |  D\n" +
                "E  | |  E\n";
        Board<Character> b1 = new BattleShipBoard<>(3, 5, 'X');
        boardCheckerHelper(b1, expectedHeader, expectedOwnBody, expectedEnemyBody);
    }

    @Test
    public void test_display_4by3 () {
        String expectedHeader = "  0|1|2|3\n";
        String expectedOwnBody =
                "A  | | |  A\n" +
                "B  | | |  B\n" +
                "C  | | |  C\n";
        String expectedEnemyBody =
                "A  | | |  A\n" +
                "B  | | |  B\n" +
                "C  | | |  C\n";
        Board<Character> b1 = new BattleShipBoard<>(4, 3, 'X');
        boardCheckerHelper(b1, expectedHeader, expectedOwnBody, expectedEnemyBody);

        expectedOwnBody =
                "A  | | |  A\n" +
                "B  |r| |  B\n" +
                "C  | | |  C\n";
        expectedEnemyBody =
                "A  | | |  A\n" +
                "B  | | |  B\n" +
                "C  | | |  C\n";
        b1.tryAddShip(new RectangleShip<>(new Placement("b1x"), 'r', '*'));
        boardCheckerHelper(b1, expectedHeader, expectedOwnBody, expectedEnemyBody);

        b1.fireAt(new Coordinate(1, 1));
        b1.fireAt(new Coordinate(1, 2));
        expectedOwnBody =
                "A  | | |  A\n" +
                "B  |*| |  B\n" +
                "C  | |r|  C\n";
        expectedEnemyBody =
                "A  | | |  A\n" +
                "B  |r|X|  B\n" +
                "C  | | |  C\n";
        b1.tryAddShip(new RectangleShip<>(new Placement("c2x"), 'r', '*'));
        boardCheckerHelper(b1, expectedHeader, expectedOwnBody, expectedEnemyBody);

    }

    /**
     * used to check if the expected output meet the board b1
     * @param b1 the board used to display
     * @param expectedHeader the header
     * @param expectedOwnBody the body used to display ships on board
     */
    private void boardCheckerHelper(Board<Character> b1, String expectedHeader, String expectedOwnBody, String expectedEnemyBody){
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        assertEquals(expectedHeader + expectedOwnBody + expectedHeader, view.displayMyOwnBoard());
        assertEquals(expectedHeader + expectedEnemyBody + expectedHeader, view.displayEnemyBoard());
    }

    /**
     * test if an invalid board size can be used to build a board
     */
    @Test
    public void test_invalid_board_size () {
        Board<Character> wideBoard = new BattleShipBoard<>(11, 20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<>(10, 27, 'X');
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }

    @Test
    public void test_displayMyBoardWithEnemyNextToIt () throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("BoardTextView_displayTwoBoards_output.txt");
        assert stream != null;
        byte[] Bytes = new byte[1600];
        int result = stream.read(Bytes);
        assert result != -1;
        String expect = new String(Bytes).trim();

        Board<Character> myB = new BattleShipBoard<>(10, 20, 'X');
        BoardTextView myV = new BoardTextView(myB);
        Board<Character> eB = new BattleShipBoard<>(10, 20, 'X');
        BoardTextView eV = new BoardTextView(eB);
        assertEquals(expect.replace("\r", ""), myV.displayMyBoardWithEnemyNextToIt(eV, "Your ocean", "Player B's ocean").trim().replace("/r", ""));
    }

}