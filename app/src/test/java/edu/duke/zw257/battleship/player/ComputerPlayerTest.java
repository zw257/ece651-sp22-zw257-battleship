package edu.duke.zw257.battleship.player;

import edu.duke.zw257.battleship.board.BattleShipBoard;
import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.V2ShipFactory;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class ComputerPlayerTest {


    private ComputerPlayer createTextPlayer(int w, int h, OutputStream bytes) {
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<>(w, h, 'X');
        V2ShipFactory shipFactory = new V2ShipFactory();
        return new ComputerPlayer("A", board, output, shipFactory);
    }

    @Test
//    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_playOneturn() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ComputerPlayer player = createTextPlayer(10, 20, bytes);
        ComputerPlayer enemyPlayer = createTextPlayer(10, 20, bytes);

    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_read_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ComputerPlayer player = createTextPlayer(10, 20, bytes);

        String prompt = "";
        assertThrows(NullPointerException.class, () -> player.readPlacement(""));
        for (int i = 0; i < 3; i++) {
            Placement p = player.readPlacement("Submarine");
            assertNotNull(p); //did we get the right Placement back
            assertEquals(prompt, bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }


    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_read_coordinate() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ComputerPlayer player = createTextPlayer(10, 20, bytes);


        for (int i = 0; i < 3; i++) {
            Coordinate c = player.readCoordinate();
            assertNotNull(c); //did we get the right Placement back
            bytes.reset(); //clear out bytes for next time around
        }

    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_doPlacementPhase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);

        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("ComputerPlayer_doPlacementPhase_output.txt");
        assertNotNull(expectedStream);

        PrintStream oldOut = System.out;

        try {
            System.setOut(out);
            ComputerPlayer player = createTextPlayer(10, 20, bytes);
            player.doPlacementPhase();
        } finally {
            System.setOut(oldOut);
        }

        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected.replace("\r", ""), actual.replace("\r", ""));
    }
}
