package edu.duke.zw257.battleship.shipFactory.ship;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.placement.Placement;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class RectangleShipTest {

    @Test
    void test_makeCoords() {
        HashSet<Coordinate> result = RectangleShip.makeCoords(new Coordinate("A0"), 2, 3);
        assertTrue(result.contains(new Coordinate("A0")));
        assertTrue(result.contains(new Coordinate("A1")));
        assertTrue(result.contains(new Coordinate("B0")));
        assertTrue(result.contains(new Coordinate("B1")));
        assertTrue(result.contains(new Coordinate("C0")));
        assertTrue(result.contains(new Coordinate("C1")));
        assertFalse(result.contains(new Coordinate("A2")));
        assertFalse(result.contains(new Coordinate("D1")));
    }

    @Test
    void test_constructor() {
        RectangleShip<Character> rs = new RectangleShip<>("testship", new Placement("A0x"), 2, 1, 'r', '*');
        assertTrue(rs.occupiesCoordinates(new Coordinate("A0")));
        assertTrue(rs.occupiesCoordinates(new Coordinate("A1")));
        assertFalse(rs.occupiesCoordinates(new Coordinate("A2")));
        assertFalse(rs.occupiesCoordinates(new Coordinate("B0")));
        assertFalse(rs.occupiesCoordinates(new Coordinate("B1")));
        assertEquals("testship", rs.getName());
    }

    @Test
    void test_recordHitAt_wasHitAt_isSunk() {
        RectangleShip<Character> rs = new RectangleShip<>("testship", new Placement("A0x"), 2, 1, 'r', '*');
        assertFalse(rs.isSunk());
        assertFalse(rs.wasHitAt(new Coordinate("A0")));
        assertFalse(rs.wasHitAt(new Coordinate("A1")));
        assertThrows(IllegalArgumentException.class, () -> rs.wasHitAt(new Coordinate("A2")));
        assertThrows(IllegalArgumentException.class, () -> rs.recordHitAt(new Coordinate("A2")));
        rs.recordHitAt(new Coordinate("A0"));
        assertTrue(rs.wasHitAt(new Coordinate("A0")));
        rs.recordHitAt(new Coordinate("a1"));
        assertTrue(rs.wasHitAt(new Coordinate("a1")));
        assertTrue(rs.isSunk());
    }

    @Test
    void test_getCoordinate() {
        RectangleShip<Character> rs = new RectangleShip<>(new Placement("A0x"), 'r', '*');
        for (Coordinate c : rs.getCoordinates()) {
            assertEquals(new Coordinate(0, 0), c);
        }
    }
}