package edu.duke.zw257.battleship.board.checker;

import edu.duke.zw257.battleship.board.BattleShipBoard;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.V1ShipFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InBoundsRuleCheckerTest {

    @Test
    void test_checkMyRule() {
        String inBoundsRuleCheckerFailMSG = "That placement is invalid: the ship goes off the board!\n";
        V1ShipFactory f = new V1ShipFactory();
        BattleShipBoard<Character> board = new BattleShipBoard<>(10, 20, new InBoundsRuleChecker<>(null), 'X');
        assertNull(board.tryAddShip(f.makeBattleship(new Placement("A0H"))));
        assertNull(board.tryAddShip(f.makeBattleship(new Placement("A6H"))));
        assertEquals(inBoundsRuleCheckerFailMSG, board.tryAddShip(f.makeBattleship(new Placement("B7H"))));
        assertNull(board.tryAddShip(f.makeBattleship(new Placement("Q0v"))));
        assertEquals(inBoundsRuleCheckerFailMSG, board.tryAddShip(f.makeBattleship(new Placement("r0v"))));
    }
}