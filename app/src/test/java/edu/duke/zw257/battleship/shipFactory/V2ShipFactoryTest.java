package edu.duke.zw257.battleship.shipFactory;

import edu.duke.zw257.battleship.placement.Placement;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class V2ShipFactoryTest {

    @Test
    void test_invalid_ship() {
        V2ShipFactory f = new V2ShipFactory();
        assertThrows(IllegalArgumentException.class, () -> f.makeCarrier(new Placement("a0v")));
        assertThrows(IllegalArgumentException.class, () -> f.makeBattleship(new Placement("a0v")));
        assertThrows(IllegalArgumentException.class, () -> f.makeDestroyer(new Placement("a0r")));
        assertThrows(IllegalArgumentException.class, () -> f.makeSubmarine(new Placement("a0r")));

    }

}