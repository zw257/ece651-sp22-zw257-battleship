package edu.duke.zw257.battleship.util;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

    @Test
    void test_readInputStrWithinRange () throws IOException {
        HashSet<String> op = new HashSet<>();
        op.add("A");
        assertThrows(Exception.class, () -> Utils.readInputStrWithinRange(null, null, null));
        assertEquals("A", Utils.readInputStrWithinRange(new BufferedReader(new StringReader("a\n")), System.out, op));

    }

    @Test
    void test_printStream() {
        assertThrows(IOException.class, () -> Utils.printStream(null, null));
    }

    @Test
    void test_readInputStream() {
        assertThrows(IOException.class, () -> Utils.readInputStr(new BufferedReader(new StringReader(""))));
    }
}