package edu.duke.zw257.battleship.player;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.board.BattleShipBoard;
import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.shipFactory.V2ShipFactory;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class TextPlayerTest {
    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<>(w, h, 'X');
        V2ShipFactory shipFactory = new V2ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }


    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_read_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n\n", bytes);

        String prompt = "";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (Placement placement : expected) {
            Placement p = player.readPlacement();
            assertEquals(p, placement); //did we get the right Placement back
            assertEquals(prompt, bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
        assertThrows(IOException.class, () -> player.readPlacement());
        assertThrows(EOFException.class, () -> player.readPlacement());
    }


    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_read_coordinate() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2\nC8\na4\n\n", bytes);

        Coordinate[] expected = new Coordinate[3];
        expected[0] = new Coordinate(1, 2);
        expected[1] = new Coordinate(2, 8);
        expected[2] = new Coordinate(0, 4);

        for (Coordinate cExpected : expected) {
            Coordinate c = player.readCoordinate();
            assertEquals(cExpected, c); //did we get the right Placement back
            bytes.reset(); //clear out bytes for next time around
        }
        assertThrows(IOException.class, () -> player.readCoordinate());
        assertThrows(EOFException.class, () -> player.readCoordinate());
    }





    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_doPlacementPhase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);

        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("TextPlayer_doPlacementPhase_output.txt");
        assertNotNull(expectedStream);

        PrintStream oldOut = System.out;

        try {
            System.setOut(out);
            TextPlayer player = createTextPlayer(10, 20, "a0v\n" +
                    "a1v\n" +
                    "a2v\n" +
                    "a3v\n" +
                    "a4v\n" +
                    "a5v\n" +
                    "t0l\n" +
                    "g0u\n" +
                    "g3u\n" +
                    "g6u\n" +
                    "j0u\n" +
                    "j3u\n" +
                    "j6u\n", bytes);
            player.doPlacementPhase();
        }
        finally {
            System.setOut(oldOut);
        }

        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected.replace("\r",""), actual.replace("\r",""));
    }


}