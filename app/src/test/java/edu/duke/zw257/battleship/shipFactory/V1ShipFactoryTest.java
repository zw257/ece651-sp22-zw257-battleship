package edu.duke.zw257.battleship.shipFactory;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class V1ShipFactoryTest {

    private void checkShip(Ship<Character> testShip, String expectedName,
                           char expectedLetter, Coordinate... expectedLocs){
        assertEquals(expectedName, testShip.getName());
        for (Coordinate loc : expectedLocs) {
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(loc, true));
            assertNull(testShip.getDisplayInfoAt(loc, false));
        }
    }

    @Test
    void test_ships_makers() {
        V1ShipFactory f = new V1ShipFactory();

        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        checkShip(f.makeSubmarine(v1_2), "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));

        Placement v2_1 = new Placement(new Coordinate(1, 2), 'h');
        checkShip(f.makeSubmarine(v2_1), "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));

        Placement v1_3 = new Placement(new Coordinate(1, 2), 'V');
        checkShip(f.makeDestroyer(v1_3), "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));

        Placement v3_1 = new Placement(new Coordinate(1, 2), 'h');
        checkShip(f.makeDestroyer(v3_1), "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));

        Placement v1_4 = new Placement(new Coordinate(1, 2), 'V');
        checkShip(f.makeBattleship(v1_4), "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2));

        Placement v4_1 = new Placement(new Coordinate(1, 2), 'h');
        checkShip(f.makeBattleship(v4_1), "Battleship", 'b', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5));

        Placement v1_6 = new Placement(new Coordinate(1, 2), 'V');
        checkShip(f.makeCarrier(v1_6), "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2), new Coordinate(5, 2), new Coordinate(6, 2));

        Placement v6_1 = new Placement(new Coordinate(1, 2), 'h');
        checkShip(f.makeCarrier(v6_1), "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5), new Coordinate(1, 6), new Coordinate(1, 7));

        Placement fakePlacement = new Placement(new Coordinate(1, 2), 'f');
        assertThrows(IllegalArgumentException.class, () -> checkShip(f.makeCarrier(fakePlacement), "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5), new Coordinate(1, 6), new Coordinate(1, 7)));

    }


}