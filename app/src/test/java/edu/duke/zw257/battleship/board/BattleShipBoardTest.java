package edu.duke.zw257.battleship.board;

import static org.junit.jupiter.api.Assertions.*;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.V2ShipFactory;
import edu.duke.zw257.battleship.shipFactory.ship.RectangleShip;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;
import org.junit.jupiter.api.Test;

class BattleShipBoardTest {
    @Test
    public void test_width_and_height () {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }

    @Test
    public void test_invalid_dimensions () {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, -5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(-8, 20, 'X'));
    }

    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expect){
        for (int y = 0; y < expect.length; ++ y) {
            for (int x = 0; x < expect[0].length; ++ x) {
                assertEquals(expect[y][x], b.whatIsAtForSelf(new Coordinate(y, x)) );
            }
        }
    }


    @Test
    public void test_tryAddShip() {
        Character[][] expected = new Character[10][20];
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        checkWhatIsAtBoard(b1, expected);

        assertNull(b1.whatIsAtForSelf(new Coordinate(0, 0)));
        expected[0][0] = 'r';
        b1.tryAddShip(new RectangleShip<>(new Placement("A0x"), 'r', '*'));
        assertEquals('r', b1.whatIsAtForSelf(new Coordinate(0, 0)));
        checkWhatIsAtBoard(b1, expected);


        assertNull(b1.whatIsAtForSelf(new Coordinate(1, 1)));
        expected[1][1] = 'r';
        b1.tryAddShip(new RectangleShip<>(new Placement("b1x"), 'r', '*'));
        assertEquals('r', b1.whatIsAtForSelf(new Coordinate(1, 1)));
        checkWhatIsAtBoard(b1, expected);
    }

    @Test
    public void test_fireAt() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        b1.tryAddShip(new RectangleShip<>("testShip" , new Placement("A0x"), 2, 2, 'r', '*'));

        assertNull(b1.whatIsAtForEnemy(new Coordinate(2, 2)));
        assertNull(b1.fireAt(new Coordinate(2, 2)));
        //miss
        b1.fireAt(new Coordinate(2, 2));
        assertEquals('X', b1.whatIsAtForEnemy(new Coordinate(2, 2)));

        Ship<Character> ship_to_fire = b1.whatShipAt(new Coordinate(1, 1));
        assertFalse(ship_to_fire.isSunk());
        assertFalse(b1.getIsLose());

        // hit
        assertNotNull(ship_to_fire);
        assertSame(ship_to_fire, b1.fireAt(new Coordinate(0, 0)));
        assertSame(ship_to_fire, b1.fireAt(new Coordinate(1, 1)));
        assertSame(ship_to_fire, b1.fireAt(new Coordinate(1, 0)));
        assertSame(ship_to_fire, b1.fireAt(new Coordinate(0, 1)));
        assertTrue(ship_to_fire.isSunk());
        assertTrue(b1.getIsLose());
    }

    @Test
    public void test_moveShipCountDown() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        assertEquals(b1.getShipMovementRemaining(), 3);
        b1.moveShipCountDown();
        assertEquals(b1.getShipMovementRemaining(), 2);
    }

    @Test
    public void test_sonarCountDown() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        assertEquals(b1.getSonarRemaining(), 3);
        b1.sonarCountDown();
        assertEquals(b1.getSonarRemaining(), 2);
    }

    @Test
    public void test_tryMoveShip () {
        V2ShipFactory f = new V2ShipFactory();
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        b1.tryAddShip(f.makeCarrier(new Placement("d0r")));
        assertThrows(Exception.class, () -> b1.tryMoveShip(null, null));
        b1.tryAddShip(f.makeCarrier(new Placement("A0r")));
        assertNull(b1.tryMoveShip(b1.whatShipAt(new Coordinate("A0")), f.makeCarrier(new Placement("c5r"))));
        assertNotNull(b1.tryMoveShip(b1.whatShipAt(new Coordinate("A0")), f.makeCarrier(new Placement("d0r"))));
    }
}