package edu.duke.zw257.battleship.player;

import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.V2ShipFactory;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;
import edu.duke.zw257.battleship.util.Utils;
import edu.duke.zw257.battleship.view.BoardTextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.function.Function;

public abstract class Player {
    protected final String name;
    protected final Board<Character> board;
    protected final BoardTextView view;
    protected final BufferedReader inputReader;
    protected final PrintStream out;
    protected final V2ShipFactory shipFactory;
    protected final ArrayList<String> shipsToPlace;
    protected final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;


    /**
     * TextPlayer constructor
     * @param name the name of this player
     * @param board the board
     * @param inputReader the input sources (reader stream)
     * @param out the output sources
     * @param shipFactory factory of ship
     */
    public Player(String name, Board<Character> board, BufferedReader inputReader, PrintStream out, V2ShipFactory shipFactory) {
        this.name = name;
        this.board = board;
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.view = new BoardTextView(board);
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        setupShipCreationMap();
        setupShipCreationList();
    }

    /**
     * setup Ship Creation Map
     */
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    }


    /**
     * setup Ship Creation List
     */
    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }


    /**
     * call utilities to print a file in the sources directories
     * @param fileName is the file name
     * @throws IOException for fail to read this file
     */
    public void printFile(String fileName) throws IOException {
        Utils.printStream(out, Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(fileName)));
    }

    /**
     * play one turn
     * @param enemyBoard enemy's board
     * @param enemyView enemy's view
     * @param enemyName enemy's name
     * @throws IOException for EOF
     */
    public abstract void playOneTurn(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException;


    /**
     * sonar scan helper
     * @param enemyBoard enemy's board
     * @param c the coordinate to scan
     * @return the tree map containing the ships have been found
     */
    protected TreeMap<String, Integer> sonar_scan_helper (Board<Character> enemyBoard, Coordinate c) {
        TreeMap<String, Integer> result = new TreeMap<>();
        int xC = c.getColumn();
        int yC = c.getRow();
        int xLeft = Math.max(0, xC - 3);
        int yUpper = Math.max(0, yC - 3);
        int xRight = Math.min(enemyBoard.getWidth() - 1, xC + 3);
        int yDown = Math.min(enemyBoard.getHeight() - 1, yC + 3);
        for (int x = xLeft; x <= xRight; ++ x) {
            for (int y = yUpper; y <= yDown; ++ y) {
                if (Math.abs(x - c.getColumn()) + Math.abs(y - c.getRow()) <= 3) {
                    Ship<Character> s = enemyBoard.whatShipAt(new Coordinate(y, x));
                    if (s != null) {
                        result.put(s.getName(), result.getOrDefault(s.getName(), 0) + 1);
                    }
                }
            }
        }
        return result;
    }

    /**
     * sonar scan
     * @param enemyBoard the enemy's Board
     * @param computerPlayer if it is a computer player, print information for false
     * @throws IOException EOF
     */
    protected void sonar_scan (Board<Character> enemyBoard, boolean computerPlayer) throws IOException {
        board.sonarCountDown();
        Coordinate c = null;
        while (c == null) {
            if (! computerPlayer) out.print(getName() + "Please select a place to scan: ");
            c = readCoordinate();
        }
        TreeMap<String, Integer> map = sonar_scan_helper(enemyBoard, c);
        for (String str : map.keySet()) {
            if (! computerPlayer) out.println(str + " occupy " + map.get(str) + " squares");
        }
    }

    /**
     * read coordinate from inputReader string input
     * @return Coordinate read
     * @throws IOException fail to read from inputReader
     */
    abstract protected Coordinate readCoordinate () throws IOException;

    /**
     *   (a) display the starting (empty) board
     *   (b) print the instructions message (from the README,
     *        but also shown again near the top of this file)
     *   (c) call doOnePlacement to place one ship
     * @throws IOException by doOnePlacement
     */
    abstract public void doPlacementPhase() throws IOException;

    public String getName() {
        return "Player " + name;
    }

    public Board<Character> getBoard() {
        return board;
    }

    public BoardTextView getView() {
        return view;
    }
}
