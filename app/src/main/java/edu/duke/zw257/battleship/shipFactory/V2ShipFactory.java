package edu.duke.zw257.battleship.shipFactory;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.displayInfo.SimpleShipDisplayInfo;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.ship.RectangleShip;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;
import edu.duke.zw257.battleship.shipFactory.ship.nonRectangleShip;


import java.util.HashMap;
import java.util.HashSet;

public class V2ShipFactory extends V1ShipFactory {
    private static final HashSet<Character> validOrientation_4 = new HashSet<>() {
        {
            add('U');
            add('R');
            add('D');
            add('L');
        }
    };
    private static final HashMap<Character, Integer> rotateTable = new HashMap<>() {
        {
            put('U', 0);
            put('R', 1);
            put('D', 2);
            put('L', 3);
            put('V', 0);
            put('H', 1);
        }
    };

    /**
     * refactor this ship to the new placement required when create or replace a ship
     * @param s ship to refactor
     * @param newPlacement new placement
     * @return new ship after refactor
     */
    public Ship<Character> ship_refactor(Ship<Character> s, Placement newPlacement) {

        int counter = rotateTable.get(newPlacement.getOrientation()) - rotateTable.get(s.getPlacement().getOrientation());
        counter = (counter + 4) % 4;
        for (; counter > 0; -- counter) {
            s = rotate_ship_clockwise_90_degrees(s, newPlacement);
        }
        return move_ship(s, newPlacement);
    }

    /**
     * rotate_ship_clockwise_90_degrees
     * @param s original ship
     * @return new ship after rotation
     */
    protected Ship<Character> rotate_ship_clockwise_90_degrees(Ship<Character> s, Placement newP) {
        Coordinate oriCoor = s.getPlacement().getCoordinate();
        // get upperLeft
        int xUpperLeft = oriCoor.getColumn();
        int yUpperLeft = oriCoor.getRow();
        // build pieces
        HashMap<Coordinate, Boolean> pieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c : s.getCoordinates()) {
            // rotate each coordinate
            int xRelativeOrigin = c.getColumn() - xUpperLeft;
            int yRelativeOrigin = c.getRow() - yUpperLeft;
            int xRelativeNew = s.getHeight() - 1 - yRelativeOrigin;
            int xNew = xUpperLeft + xRelativeNew;
            int yNew = yUpperLeft + xRelativeOrigin;
            pieces.put(new Coordinate(yNew, xNew), s.wasHitAt(c));
        }
        // build new ship
        Ship<Character> newShip = null;
        if (s instanceof RectangleShip) {
            newShip = new RectangleShip<>(s.getName(), new Placement(oriCoor, newP.getOrientation()), pieces, (SimpleShipDisplayInfo<Character>) s.getMyDisplayInfo(), (SimpleShipDisplayInfo<Character>) s.getEnemyDisplayInfo());
        } else if (s instanceof nonRectangleShip) {
            newShip = new nonRectangleShip<>(s.getName(), new Placement(oriCoor, newP.getOrientation()), pieces, (SimpleShipDisplayInfo<Character>) s.getMyDisplayInfo(), (SimpleShipDisplayInfo<Character>)s.getEnemyDisplayInfo());
        }
        return newShip;
    }

    /**
     * move the ship to another location
     */
    protected Ship<Character> move_ship (Ship<Character> s, Placement newP) {
        char origOrientation = s.getPlacement().getOrientation();
        int xOffset = newP.getCoordinate().getColumn() - s.getPlacement().getCoordinate().getColumn();
        int yOffset = newP.getCoordinate().getRow() - s.getPlacement().getCoordinate().getRow();
        HashMap<Coordinate, Boolean> pieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c : s.getCoordinates()) {
            pieces.put(new Coordinate(c.getRow() + yOffset, c.getColumn() + xOffset), s.wasHitAt(c));
        }
        // build new ship
        Ship<Character> newShip = null;
        if (s instanceof RectangleShip) {
            newShip = new RectangleShip<>(s.getName(), new Placement(newP.getCoordinate(), origOrientation), pieces, (SimpleShipDisplayInfo<Character>) s.getMyDisplayInfo(), (SimpleShipDisplayInfo<Character>) s.getEnemyDisplayInfo());
        } else if (s instanceof nonRectangleShip) {
            newShip = new nonRectangleShip<>(s.getName(), new Placement(newP.getCoordinate(), origOrientation), pieces, (SimpleShipDisplayInfo<Character>) s.getMyDisplayInfo(), (SimpleShipDisplayInfo<Character>)s.getEnemyDisplayInfo());
        }
        return newShip;
    }

    /**
     * createShip
     * @param letter the letter for display
     * @param name is the name of this ship
     * @return a NonRectangleShip
     */
    protected Ship<Character> createNonRectangleShip(Placement placement, HashSet<Coordinate> pieces, char letter, String name){
        return new nonRectangleShip<>(name, placement, pieces, letter, '*');
    }

    /**
     * Make a battleship.
     * which has size 1 * 4
     * @param placement specifies the location and orientation of the ship to make
     * @return the Ship created for the battleship.
     *
     *   Battleships:
     *        *b      OR    B         Bbb        *b
     *        bbb           bb   OR    b     OR  bb
     *                      b                     b
     *
     *         Up          Right      Down      Left
     *
     */
    @Override
    public Ship<Character> makeBattleship(Placement placement) {
        if (! validOrientation_4.contains(placement.getOrientation())) {
            throw new IllegalArgumentException("Invalid orientation, which is " + placement.getOrientation());
        }
        HashSet<Coordinate> pieces = new HashSet<>() {
            { // generate a ship with Up orientation as default
                add(new Coordinate(0, 1));
                add(new Coordinate(1, 0));
                add(new Coordinate(1, 1));
                add(new Coordinate(1, 2));
            }
        };
        Ship<Character> s =  createNonRectangleShip(new Placement(new Coordinate(0, 0), 'U'), pieces, 'b', "Battleship");
        return ship_refactor(s, placement);
    }

    /**
     * Make a carrier.
     * which has size 1 * 6
     * @param placement specifies the location and orientation of the ship to make
     * @return the Ship created for the carrier.
     *
     *   Carriers:
     *            C                       C
     *            c          *cccc        cc       * ccc
     *            cc   OR    ccc      OR  cc   OR  cccc
     *            cc                       c
     *             c                       c
     *
     *           Up           Right     Down          Left
     *
     */
    @Override
    public Ship<Character> makeCarrier(Placement placement) {
        if (! validOrientation_4.contains(placement.getOrientation())) {
            throw new IllegalArgumentException("Invalid orientation, which is " + placement.getOrientation());
        }
        HashSet<Coordinate> pieces = new HashSet<>() {
            { // generate a ship with Up orientation as default
                add(new Coordinate(0, 0));
                add(new Coordinate(1, 0));
                add(new Coordinate(2, 0));
                add(new Coordinate(3, 0));
                add(new Coordinate(2, 1));
                add(new Coordinate(3, 1));
                add(new Coordinate(4, 1));
            }
        } ;
        Ship<Character> s =  createNonRectangleShip(new Placement(new Coordinate(0, 0), 'U'), pieces, 'c', "Carrier");
        return ship_refactor(s, placement);
    }

}
