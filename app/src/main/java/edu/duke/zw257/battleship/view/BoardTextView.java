package edu.duke.zw257.battleship.view;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.board.Board;

import java.util.function.Function;

/**
 * This class handles textual display of a Board (i.e., converting it to a string to show to the user).
 * It support two ways to display the Board;
 * One for the player's own board, and one for the enemy's board.
 *
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Construts a BoardView, given the board it will display.
     * @param toDisplay is the Board to display.
     */
    public BoardTextView (Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight()
            );
        }
    }

    /**
     * this method used to display the board as string
     * @return a string would be returned for print
     */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder();
        ans.append(makeHeader());
        ans.append(makeBody(getSquareFn));
        ans.append(makeHeader());
        return ans.toString(); // this is a placeholder for the monet
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     * @return the String that is the header line for the given board
     */
    String makeHeader () {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep = ""; // start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep).append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }

    /**
     * This makes the body lines, e.g. A\n  B\n  C\n
     *
     * @return the String that is the body lines for the given board
     */
    String makeBody (Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder();
        for (int h = 0; h < toDisplay.getHeight(); h ++) {
            String sep = "";
            ans.append((char)(h + 'A'));
            StringBuilder grid = new StringBuilder(" ");
            for (int w = 0; w < toDisplay.getWidth(); w ++) {
                grid.append(sep);
                if (getSquareFn.apply(new Coordinate(h, w)) != null) {
                    grid.append(getSquareFn.apply(new Coordinate(h, w)));
                } else {
                    grid.append(" ");
                }
                sep = "|";
            }
            ans.append(grid).append(" ").append((char)(h + 'A')).append("\n");
        }
        return ans.toString();
    }

    /**
     * display my own board
     * @return the board to display
     */
    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }

    /**
     * display the enemy board
     * @return the board to display
     */
    public String displayEnemyBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }

    /**
     * display My Board With Enemy Next To It
     * @param enemyView the enemyView
     * @param myHeader my header
     * @param enemyHeader my enemy header
     * @return the string to display
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        StringBuilder ans = new StringBuilder();
        StringBuilder spaceBetween = new StringBuilder();
        spaceBetween.append(" ".repeat((int)(this.toDisplay.getWidth() * 1.6)));
        String [] myBoard = displayMyOwnBoard().split("\n");
        String [] enemyBoard = enemyView.displayEnemyBoard().split("\n");
        // build header
        ans.append(" ".repeat(5)).append(myHeader).append(" ".repeat(this.toDisplay.getWidth() * 2 - 9)).append(spaceBetween).append(enemyHeader).append("\n");
        // build body
        ans.append(myBoard[0]).append("  ").append(spaceBetween).append(enemyBoard[0]).append("\n");
        for (int i = 1; i < myBoard.length - 1; ++ i) {
            ans.append(myBoard[i]).append(spaceBetween).append(enemyBoard[i]).append("\n");
        }
        ans.append(myBoard[0]).append("  ").append(spaceBetween).append(enemyBoard[0]).append("\n");
        return ans.toString();
    }


}

