package edu.duke.zw257.battleship.shipFactory;

import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.ship.RectangleShip;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;

public class V1ShipFactory implements AbstractShipFactory<Character> {

    /**
     * createShip
     * @param where Placement of this ship
     * @param w width of this ship
     * @param h height of this ship
     * @param letter the letter for display
     * @param name the name of this ship
     * @return the ship asked for constructed
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
        return new RectangleShip<>(name, where, w, h, letter, '*');
    }

    /**
     * check the Direction of this ship, vertical or horizontal
     * @param where the placement
     * @param size pass in the vertical placement of this ship by default
     * @return the size after changing the direction
     */
    protected int[] checkDirection (Placement where, int[] size){
        if (where.getOrientation() == 'V') {
            return size;
        }
        if (where.getOrientation() == 'H') {
            return new int[]{size[1], size[0]};
        }
        throw new IllegalArgumentException("Invalid orientation, which is " + where.getOrientation());
    }



    /**
     * Make a submarine.
     * which has size 1 * 2
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine.
     */
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        int[] size = checkDirection(where, new int[]{1, 2});
        return createShip(where, size[0], size[1], 's', "Submarine");
    }

    /**
     * Make a battleship.
     * which has size 1 * 4
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the battleship.
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        int[] size = checkDirection(where, new int[]{1, 4});
        return createShip(where, size[0], size[1], 'b', "Battleship");
    }

    /**
     * Make a carrier.
     * which has size 1 * 6
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the carrier.
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        int[] size = checkDirection(where, new int[]{1, 6});
        return createShip(where, size[0], size[1], 'c', "Carrier");
    }

    /**
     * Make a destroyer.
     * which has size 1 * 3
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the destroyer.
     */
    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        int[] size = checkDirection(where, new int[]{1, 3});
        return createShip(where, size[0], size[1], 'd', "Destroyer");
    }
}
