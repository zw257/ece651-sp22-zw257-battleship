package edu.duke.zw257.battleship.board.checker;

import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

    /**
     * constructor of the in bourds rule checker
     * @param next the next node in this chain
     */
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * the checking rule's detail
     *
     * @param theShip  is the ship to check
     * @param theBoard is the board to place
     * @return null for success, and return string for failure
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (c.getRow() >= theBoard.getHeight() || c.getColumn() >= theBoard.getWidth()) {
                return "That placement is invalid: the ship goes off the board!\n";
            }
        }
        return null;
    }


}
