package edu.duke.zw257.battleship.board.checker;

import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T>{

    /**
     * the constructor of No collision rule checker
     * @param next is the next node in this chain
     */
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * the checking rule's detail
     *
     * @param theShip  is the ship to check
     * @param theBoard is the board to place
     * @return null for success, and return string for failure
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(c) != null) {
                return "That placement is invalid: the ship overlaps another ship.\n";
            }
        }
        return null;
    }
}
