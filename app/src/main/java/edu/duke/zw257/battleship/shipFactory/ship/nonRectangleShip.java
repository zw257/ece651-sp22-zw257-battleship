package edu.duke.zw257.battleship.shipFactory.ship;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.displayInfo.SimpleShipDisplayInfo;
import edu.duke.zw257.battleship.placement.Placement;

import java.util.HashMap;
import java.util.HashSet;

public class nonRectangleShip<T> extends BasicShip<T> {
    protected final String name;


    /**
     * constructor of a rectangle ship
     * @param name name of this ship
     * @param pieces a map fo coordinates and booleans indicate the place of this ship
     * @param myDisplayInfo my displayInfo of this ship
     * @param enemyDisplayInfo enemy displayInfo of this ship
     */
    public nonRectangleShip(String name, Placement placement,
                            HashMap<Coordinate, Boolean> pieces,
                            SimpleShipDisplayInfo<T> myDisplayInfo,
                            SimpleShipDisplayInfo<T> enemyDisplayInfo) {
        super(placement, pieces, myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * constructor of a rectangle ship
     * @param name name of this ship
     * @param coordinates a collection fo coordinates indicate the place of this ship
     * @param myDisplayInfo my displayInfo of this ship
     * @param enemyDisplayInfo enemy displayInfo of this ship
     */
    public nonRectangleShip(String name, Placement placement,
                            Iterable<Coordinate> coordinates,
                            SimpleShipDisplayInfo<T> myDisplayInfo,
                            SimpleShipDisplayInfo<T> enemyDisplayInfo) {
        super(placement, coordinates, myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * constructor of a rectangle ship
     * @param name name of this ship
     * @param coordinates a collection fo coordinates indicate the place of this ship
     * @param data the data to display for this ship
     * @param onHit the data to display for this when hit
     */
    public nonRectangleShip(String name, Placement placement,
                            Iterable<Coordinate> coordinates, T data, T onHit) {
        this(name, placement, coordinates,
        new SimpleShipDisplayInfo<>(data, onHit),
        new SimpleShipDisplayInfo<>(null, data));
    }


    /**
     * Get the name of this Ship, such as "submarine".
     * @return the name of this ship
     */
    @Override
    public String getName() {
        return this.name;
    }


}
