package edu.duke.zw257.battleship.player;

import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.V2ShipFactory;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;
import edu.duke.zw257.battleship.util.Utils;
import edu.duke.zw257.battleship.view.BoardTextView;

import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.function.Function;

public class ComputerPlayer extends Player{
    private final ArrayList<Character> V_H ;
    private final ArrayList<Character> U_R_D_L ;
    private final Map<String, List<Character>> shipDirections ;
    // recordingMode is used to record the raw data input from computer player

    /**
     * ComputerPlayer constructor
     * @param name is the name of this computer player
     * @param board the board
     * @param out the output of this computer player
     * @param shipFactory the ship factory
     */
    public ComputerPlayer(String name, Board<Character> board, PrintStream out, V2ShipFactory shipFactory) {
        super(name, board, null, out, shipFactory);
        this.V_H = new ArrayList<>()  {
            {
                add('H');
                add('V');
            }
        };
        this.U_R_D_L = new ArrayList<>()  {
            {
                add('U');
                add('R');
                add('D');
                add('L');
            }
        };
        this.shipDirections = new TreeMap<>(){
            {
                put("Submarine", V_H);
                put("Destroyer", V_H);
                put("Carrier", U_R_D_L);
                put("Battleship", U_R_D_L);
            }
        };
    }

    /**
     * Auto do placement phase
     * @throws IOException by doOnePlacement
     */
    public void doPlacementPhase() throws IOException{
        view.displayMyOwnBoard();
        for (String s : shipsToPlace) {
            doOnePlacement(s, shipCreationFns.get(s));
            this.out.print(getName() + " has placed a " + s + "\n");
        }
    }

    /**
     * auto do one placement
     * @param shipName the name of this ship
     * @param createFn is the function to create ships
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException{
        Ship<Character> s = parsePlacementToShip (shipName, createFn);

        String placementFailure = board.tryAddShip(s);
        if (placementFailure != null) {
            doOnePlacement(shipName, createFn);
        }
    }

    /**
     * parse a Placement a To a Ship
     * @param shipName the name of this ship
     * @param createFn is the function to create ships
     * @return a ship for adding
     * @throws IOException for EOFException
     */
    public Ship<Character> parsePlacementToShip (String shipName, Function<Placement, Ship<Character>> createFn) throws IOException{
        Placement p = readPlacement(shipName);
        return createFn.apply(p);
    }

    /**
     * read input stream and parse to Placement
     * @return  a Placement based on user input
     */
    public Placement readPlacement (String shipName) {
        return placementGenerator(shipName);
    }

    /**
     * auto-play one turn
     * @param enemyBoard enemy's board
     * @param enemyView enemy's view
     */
    public void playOneTurn (Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        // auto select the options
        int op = Utils.randomGenerator(5);
        switch (op) {
            case 0:
                if (getBoard().getShipMovementRemaining() <= 0) {
                    fire_at_a_Square(enemyBoard);
                } else {
                    out.print(getName() + " used a special action\n");
                    move_ship_to_another_square();
                }
                break;
            case 1:
                if (getBoard().getSonarRemaining() <= 0) {
                    fire_at_a_Square(enemyBoard);
                } else {
                    out.print(getName() + " used a special action\n");
                    sonar_scan(enemyBoard, true);
                }
            default:
                fire_at_a_Square(enemyBoard);
                break;
        }
    }

    /**
     * Move_ship_to_another_square
     */
    protected void move_ship_to_another_square ()  {
        getBoard().moveShipCountDown();
        String movementWarning = " ";
        while (movementWarning != null) {
            Ship<Character> oringinalShip = null;
            while (oringinalShip == null) {
                oringinalShip = getBoard().whatShipAt(readCoordinate());
            }
            Placement newPlacement = null;
            while (newPlacement == null) {
                newPlacement = readPlacement(oringinalShip.getName());
            }
            Ship<Character> newShip = shipFactory.ship_refactor(oringinalShip, newPlacement);
            movementWarning = getBoard().tryMoveShip(oringinalShip, newShip);
        }
    }

    /**
     * F Fire at a square
     * @param enemyBoard the enemy board
     */
    protected void fire_at_a_Square(Board<Character> enemyBoard) {
        out.print(getName() + " perform a fire,");
        Ship<Character> s = enemyBoard.fireAt(readCoordinate());
        if (s != null) {
            out.println(" and hit a " + s.getName() + "!");
        } else {
            out.println(" and missed!");
        }
    }


    /**
     * auto generate coordinate
     * @return Coordinate read
     * @throws IOException fail to read from inputReader
     */
    protected Coordinate readCoordinate () {
        int y = Utils.randomGenerator(getBoard().getHeight());
        int x = Utils.randomGenerator(getBoard().getWidth());
        return new Coordinate(y, x);
    }

    /**
     * placement Generator
     * @param shipName the name of this ship to generate
     * @return the placement generated based on ship
     */
    protected Placement placementGenerator (String shipName) {
        char c = Utils.randomCharGenerator(shipDirections.get(shipName));
        Placement p = new Placement(readCoordinate(), c);
        return p;
    }

    public String getName() {
        return "Comp-Player " + name;
    }
}
