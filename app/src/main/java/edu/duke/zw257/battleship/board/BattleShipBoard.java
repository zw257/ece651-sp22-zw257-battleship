package edu.duke.zw257.battleship.board;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.board.checker.InBoundsRuleChecker;
import edu.duke.zw257.battleship.board.checker.NoCollisionRuleChecker;
import edu.duke.zw257.battleship.board.checker.PlacementRuleChecker;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;
    private final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    private final HashSet<Coordinate> enemyMisses;
    private final HashMap<Coordinate, T> enemyHit;
    private final T missInfo;
    private boolean isLost = false;
    private Integer shipMovementRemaining = 3;
    private Integer sonarRemaining = 3;



    /**
     * Constructs a BattleShipBoard with the specified width and height
     * @param width is the width of the newly constructed board
     * @param height is the height of the newly constructed board
     * @throws IllegalArgumentException if the width or height are less than or equal to zero
     */
    public BattleShipBoard (int width, int height, T missInfo) {
        this (width, height, new NoCollisionRuleChecker<>(new InBoundsRuleChecker<>(null)), missInfo);
    }

    /**
     * Constructs a BattleShipBoard with the specified width and height
     * @param width is the width of the newly constructed board
     * @param height is the height of the newly constructed board
     * @param checker is the chain of placement rule checker
     * @throws IllegalArgumentException if the width or height are less than or equal to zero
     */
    public BattleShipBoard (int width, int height, PlacementRuleChecker<T> checker, T missInfo) {
        if (width <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive bus is " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive bus is " + height);
        }
        this.myShips = new ArrayList<>();
        this.width = width;
        this.height = height;
        this.placementChecker = checker;
        this.enemyMisses = new HashSet<>();
        this.enemyHit = new HashMap<>();
        this.missInfo = missInfo;
    }

    /**
     * this function would add a ship to a specific Coordinate
     * @param toAdd the ship to add with type T
     * @return null for success, return any other for failure
     */
    @Override
    public String tryAddShip (Ship<T> toAdd) {
        String result = placementChecker.checkPlacement(toAdd, this);
        if (result == null) {
            myShips.add(toAdd);
            return null;
        }
        return result;
    }

    /**
     * this function would add a ship to a specific Coordinate
     * @param originShip the ship to move with type T
     * @newShip originShip the ship to add with type T
     * @return null for success, return any other for failure
     */
    @Override
    public String tryMoveShip (Ship<T> originShip, Ship<T> newShip) {
        myShips.remove(originShip);
        String result = tryAddShip(newShip);
        if (result == null) {
            // success
            return null;
        }
        // fail to move ship
        myShips.add(originShip);
        return result;
    }


    /**
     * this method used to search the ship at a specific Coordinate for self
     * @param where input a specific Coordinate
     * @return return the ship info or null for not found
     */
    @Override
    public T whatIsAtForSelf (Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * this method used to search the ship at a specific Coordinate for self
     * @param where input a specific Coordinate
     * @return return the ship info or null for not found
     */
    @Override
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }



    /**
     * this method used to search the ship at a specific Coordinate
     * @param where input a specific Coordinate
     * @param isSelf is a boolean for check if it is player's one call
     * @return return the ship info or null for not found
     */
    protected T whatIsAt(Coordinate where, boolean isSelf){
        if (! isSelf) {
            // enemy view
            if (enemyMisses.contains(where)) {
                return missInfo;
            }
            if (enemyHit.containsKey(where)) {
                return enemyHit.get(where);
            }
            return null;
        }
        // self view
        Ship<T> result = whatShipAt(where);
        return result != null? result.getDisplayInfoAt(where, true) : null;
    }

    /**
     * this method used to search the ship at a specific Coordinate
     *
     * @param where input a specific Coordinate
     * @return return the ship or null for not found
     */
    public Ship<T> whatShipAt (Coordinate where) {
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                return s;
            }
        }
        return null;
    }


    /**
     * This method should be called by enemy, and check for any ship that occupies coordinate c
     * @param c coordinate being shot
     * @return the ship has being shot
     */
    public Ship<T> fireAt(Coordinate c) {
        Ship<T> s = whatShipAt(c);
        if (s != null) {
            // hit
            s.recordHitAt(c);
            enemyHit.put(c, s.getDisplayInfoAt(c, false));
            checkLose();
            return s;
        }
        // miss
        enemyMisses.add(c);
        return null;
    }


    /**
     * check if self is lose
     */
    @Override
    public void checkLose() {
        for (Ship<T> s : myShips) {
            if (! s.isSunk()) {
                return;
            }
        }
        this.isLost = true;
    }

    /**
     * move ship
     * @return true for success
     */
    public void moveShipCountDown() {
        -- shipMovementRemaining;
    }

    /**
     * use sonar
     * @return true for success
     */
    public void sonarCountDown() {
        -- sonarRemaining;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public boolean getIsLose() {
        return this.isLost;
    }

    public int getShipMovementRemaining() {
        return shipMovementRemaining;
    }

    public int getSonarRemaining() {
        return sonarRemaining;
    }
}
