package edu.duke.zw257.battleship.shipFactory;


import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;

/**
 * This interface represents an Abstract Factory pattern for Ship creation.
 */
public interface AbstractShipFactory<T> {

    /**
     * Make a submarine.
     * which has size 1 * 2
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine.
     */
    Ship<T> makeSubmarine(Placement where);

    /**
     * Make a battleship.
     * which has size 1 * 4
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the battleship.
     */
    Ship<T> makeBattleship(Placement where);

    /**
     * Make a carrier.
     * which has size 1 * 6
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the carrier.
     */
    Ship<T> makeCarrier(Placement where);

    /**
     * Make a destroyer.
     * which has size 1 * 3
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the destroyer.
     */
    Ship<T> makeDestroyer(Placement where);

}

