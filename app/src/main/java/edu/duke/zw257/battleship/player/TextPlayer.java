package edu.duke.zw257.battleship.player;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.placement.Placement;
import edu.duke.zw257.battleship.shipFactory.V2ShipFactory;
import edu.duke.zw257.battleship.util.Utils;
import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;
import edu.duke.zw257.battleship.view.BoardTextView;

import java.io.*;
import java.util.*;
import java.util.function.Function;

public class TextPlayer extends Player {

    /**
     * TextPlayer constructor
     * @param name the name of this player
     * @param board the board
     * @param inputReader the input sources (reader stream)
     * @param out the output sources
     * @param shipFactory factory of ship
     */
    public TextPlayer(String name, Board<Character> board, BufferedReader inputReader, PrintStream out, V2ShipFactory shipFactory) {
        super(name, board, inputReader, out, shipFactory);
    }

    /**
     * read input stream and parse to Placement
     * @return  a Placement based on user input
     */
    public Placement readPlacement () throws IOException{
        Placement p;
        while (true) {
            try {
                p = new Placement(Utils.readInputStr(inputReader));
            } catch (IllegalArgumentException e) {
                this.out.println(e.getMessage());
                continue;
            }
            break;
        }
        return p;
    }

    /**
     * parse a Placement a To a Ship
     * @param shipName the name of this ship
     * @param createFn is the function to create ships
     * @return a ship for adding
     * @throws IOException for EOFException
     */
    public Ship<Character> parsePlacementToShip (String shipName, Function<Placement, Ship<Character>> createFn) throws IOException{
        Ship<Character> s;
        while (true) {
            try {
                this.out.print(getName() + " where do you want to place a " + shipName + "?\n");
                Placement p = readPlacement();
                s  = createFn.apply(p);
            } catch (IllegalArgumentException e) {
                this.out.println(e.getMessage());
                continue;
            }
            break;
        }
        return s;
    }

    /**
     * do one placement
     * @param shipName the name of this ship
     * @param createFn is the function to create ships
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException{
        Ship<Character> s = parsePlacementToShip (shipName, createFn);
        String placementFailure = board.tryAddShip(s);
        if (placementFailure != null) {
            this.out.print(placementFailure);
            doOnePlacement(shipName, createFn);
        }
    }

    /**
     *   (a) display the starting (empty) board
     *   (b) print the instructions message (from the README,
     *        but also shown again near the top of this file)
     *   (c) call doOnePlacement to place one ship
     * @throws IOException by doOnePlacement
     */
    public void doPlacementPhase() throws IOException{
        view.displayMyOwnBoard();
        printFile("instructionsMsgHeader");
        this.out.print(getName() + ": ");
        printFile("instructionsMsg");
        for (String s : shipsToPlace) {
            doOnePlacement(s, shipCreationFns.get(s));
            printFile("instructionsMsgHeader");
            this.out.println(view.displayMyOwnBoard());
            printFile("instructionsMsgHeader");
        }
    }


    /**
     * play one turn
     * @param enemyBoard enemy's board
     * @param enemyView enemy's view
     */
    public void playOneTurn (Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        // print instruction
        printInstructionsForEachTurn(enemyBoard, enemyView, enemyName);
        // get option chosen
        String option = getOptionInputFromUser();
        // goto the option
        switch (option) {
            case "F":
                fire_at_a_Square(enemyBoard);
                break;
            case "M":
                move_ship_to_another_square();
                this.out.println(view.displayMyOwnBoard());
                break;
            case "S":
                sonar_scan(enemyBoard, false);
                break;
        }
    }




    /**
     * Move_ship_to_another_square
     */
    protected void move_ship_to_another_square () throws IOException {
        getBoard().moveShipCountDown();
        String movementWarning = " ";
        while (movementWarning != null) {
            Ship<Character> oringinalShip = null;
            while (oringinalShip == null) {
                out.print(getName() + " Please select a ship to move: ");
                oringinalShip = getBoard().whatShipAt(readCoordinate());
            }
            Placement newPlacement = null;
            while (newPlacement == null) {
                out.print(getName() + " Please select a new placement: ");
                newPlacement = readPlacement();
            }

            Ship<Character> newShip = shipFactory.ship_refactor(oringinalShip, newPlacement);
            movementWarning = getBoard().tryMoveShip(oringinalShip, newShip);
            if (movementWarning != null) out.println(movementWarning);

        }

    }

    /**
     * F Fire at a square
     * @param enemyBoard the enemy board
     */
    protected void fire_at_a_Square(Board<Character> enemyBoard) throws IOException {
        out.print("Please type in a Coordinate you wanna fire at: ");
        Ship<Character> s = enemyBoard.fireAt(readCoordinate());
        if (s != null) {
            out.println("You hit a " + s.getName() + "!");
        } else {
            out.println("You missed!");
        }
    }

    /**
     * print instructions for each turn
     * @throws IOException for fail to print a file
     */
    protected void printInstructionsForEachTurn (Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        printFile("instructionsMsgHeader");
        // print instruction
        out.println();
        out.println(getName() + "'s turn:");
        // print board
        out.print(view.displayMyBoardWithEnemyNextToIt(enemyView, "Your ocean", enemyName + "'s ocean"));
        // print options
        printFile("instructionsMsgHeader");
        out.print ("Possible actions for " + getName() + ":\n\n");
        out.print (" F Fire at a square\n");
        if (getBoard().getShipMovementRemaining() > 0) out.print (" M Move a ship to another square (" + getBoard().getShipMovementRemaining() + " remaining)\n");
        if (getBoard().getSonarRemaining() > 0) out.print (" S Sonar scan (" + getBoard().getSonarRemaining() + " remaining)\n");
        out.print ("\n" + getName() + ", what would you like to do? ");
    }

    /**
     * get Option chosen From the User
     * @return the option has chosen
     * @throws IOException for EOF exception
     */
    protected String getOptionInputFromUser () throws IOException {
        TreeSet<String> availableOptions = new TreeSet<>();
        availableOptions.add("F");
        if (getBoard().getShipMovementRemaining() > 0) {
            availableOptions.add("M");
        }
        if (getBoard().getSonarRemaining() > 0) {
            availableOptions.add("S");
        }
        return Utils.readInputStrWithWhiteList(inputReader, out, availableOptions);
    }

    /**
     * read coordinate from inputReader string input
     * @return Coordinate read
     * @throws IOException fail to read from inputReader
     */
    protected Coordinate readCoordinate () throws IOException {
        try {
            Coordinate c = new Coordinate(Utils.readInputStr(inputReader));
            return c;
        } catch (EOFException e) {
            throw e;
        } catch (IllegalArgumentException e) {
            this.out.println(e.getMessage());
            return readCoordinate();
        }
    }

}
