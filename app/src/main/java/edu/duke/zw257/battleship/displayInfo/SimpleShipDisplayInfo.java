package edu.duke.zw257.battleship.displayInfo;

import edu.duke.zw257.battleship.coordinate.Coordinate;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    T myData;
    T onHit;

    /**
     * get info of this ship
     * @param where the coordinate to check
     * @param hit if it was hit
     * @return the element to display
     */
    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if (hit) {
            return onHit;
        }
        return myData;
    }

    /**
     * SimpleShipDisplayInfo constructor
     * @param myData the data to displayed for a ship
     * @param onHit the data to displayed for a ship when hit
     */
    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

}
