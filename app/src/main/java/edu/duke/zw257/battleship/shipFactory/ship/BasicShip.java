package edu.duke.zw257.battleship.shipFactory.ship;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.displayInfo.ShipDisplayInfo;
import edu.duke.zw257.battleship.placement.Placement;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T>{
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    protected final Placement placement;
    protected final int height;
    protected final int width;


    /**
     * BasicShip constructor
     * @param placement the placement of this basic ship
     * @param pieces the pieces of this ship
     * @param myDisplayInfo myDisplayInfo
     * @param enemyDisplayInfo enemyDisplayInfo
     */
    public BasicShip(Placement placement, HashMap<Coordinate, Boolean> pieces, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        this.myPieces = pieces;
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;

        int xDownRight = 0, yDownRight = 0;
        for (Coordinate c : myPieces.keySet()) {
            xDownRight = Math.max(xDownRight, c.getColumn());
            yDownRight = Math.max(yDownRight, c.getRow());
        }
        this.placement = placement;
        this.height = yDownRight - placement.getCoordinate().getRow() + 1;
        this.width = xDownRight - placement.getCoordinate().getColumn() + 1;
    }

    /**
     * basic ship constructor
     * @param where location to place the ship
     * @param myDisplayInfo display info of this ship
     */
    public BasicShip(Placement placement, Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        this(placement, makeHashMap(where), myDisplayInfo, enemyDisplayInfo);
    }

    /**
     * make a Hash Map based on the Coordinates to store in this.pieces
     * @param where a collection of Coordinates
     * @return the hash map
     */
    static HashMap<Coordinate, Boolean> makeHashMap (Iterable<Coordinate> where) {
        HashMap<Coordinate, Boolean> pieces = new HashMap<>();
        for (Coordinate c: where) {
            pieces.put(c, false);
        }
        return pieces;
    }

    /**
     * Check if this ship occupies the given coordinate.
     *
     * @param where is the Coordinate to check if this Ship occupies
     * @return true if where is inside this ship, false if not.
     */
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        for (Coordinate c : myPieces.keySet()) {
            if (c.equals(where)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if this ship has been hit in all of its locations meaning it has been
     * sunk.
     *
     * @return true if this ship has been sunk, false otherwise.
     */
    @Override
    public boolean isSunk() {
        return ! myPieces.containsValue(false);
    }

    /**
     * Make this ship record that it has been hit at the given coordinate. The
     * specified coordinate must be part of the ship.
     *
     * @param where specifies the coordinates that were hit.
     * @throws IllegalArgumentException if where is not part of the Ship
     */
    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    /**
     * Check if this ship was hit at the specified coordinates. The coordinates must
     * be part of this Ship.
     *
     * @param where is the coordinates to check.
     * @return true if this ship as hit at the indicated coordinates, and false
     *         otherwise.
     * @throws IllegalArgumentException if the coordinates are not part of this
     *                                  ship.
     */
    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    /**
     * Return the view-specific information at the given coordinate. This coordinate
     * must be part of the ship.
     *
     * @param where is the coordinate to return information for
     * @param isSlef
     * @throws IllegalArgumentException if where is not part of the Ship
     * @return The view-specific information at that coordinate.
     */
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean isSlef) {
        if (isSlef) {
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }
        return enemyDisplayInfo.getInfo(where, wasHitAt(where));
    }

    /**
     * checkCoordinateInThisShip
     * @param c check if the coordinate is part of this ship
     * @throws IllegalArgumentException if where is not part of the Ship
     */
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (! myPieces.containsKey(c)) throw new IllegalArgumentException("Coordinate " + c.toString() + " is not a part of this ship ");
    }


    /**
     * Get all of the Coordinates that this Ship occupies.
     *
     * @return An Iterable with the coordinates that this Ship occupies
     */
    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    public int getHeight() {
        return this.height;
    }

    public Placement getPlacement() {
        return this.placement;
    }

    public ShipDisplayInfo<T> getMyDisplayInfo() {
        return this.myDisplayInfo;
    }

    public ShipDisplayInfo<T> getEnemyDisplayInfo() {
        return this.enemyDisplayInfo;
    }
}
