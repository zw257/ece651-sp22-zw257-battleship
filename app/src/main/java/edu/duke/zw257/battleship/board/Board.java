package edu.duke.zw257.battleship.board;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;

public interface Board<T> {

    /**
     * this function would add a ship to a specific Coordinate
     * @param toAdd the ship to add with type T
     * @return null for success, and return string for failure
     */
    String tryAddShip (Ship<T> toAdd);

    /**
     * this function would add a ship to a specific Coordinate
     * @param originShip the ship to move with type T
     * @newShip originShip the ship to add with type T
     * @return null for success, return any other for failure
     */
    String tryMoveShip (Ship<T> originShip, Ship<T> newShip);


    /**
     * this method used to search the ship's data at a specific Coordinate
     * @param where input a specific Coordinate
     * @return return the ship data or null for not found
     */
    T whatIsAtForSelf(Coordinate where);

    /**
     * this method used to search the ship at a specific Coordinate for self
     * @param where input a specific Coordinate
     * @return return the ship info or null for not found
     */
    T whatIsAtForEnemy(Coordinate where);

    /**
     * This method should search for any ship that occupies coordinate c
     * @param c coordinate being shot
     * @return the ship has being shot
     */
    Ship<T> fireAt(Coordinate c);

    /**
     * check if self is lose
     */
    void checkLose();

    /**
     * this method used to search the ship at a specific Coordinate
     *
     * @param where input a specific Coordinate
     * @return return the ship or null for not found
     */
    Ship<T> whatShipAt (Coordinate where);

    /**
     * Move_ship_to_another_square
     */
    void moveShipCountDown();

    /**
     * use sonar
     * @return true for success
     */
    void sonarCountDown();

    int getWidth();

    int getHeight();

    boolean getIsLose();

    int getShipMovementRemaining();

    int getSonarRemaining();
}
