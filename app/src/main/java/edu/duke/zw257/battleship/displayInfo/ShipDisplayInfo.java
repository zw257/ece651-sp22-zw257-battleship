package edu.duke.zw257.battleship.displayInfo;

import edu.duke.zw257.battleship.coordinate.Coordinate;

public interface ShipDisplayInfo<T> {

    /**
     * get info of this ship
     * @param where the coordinate to check
     * @param hit if it was hit
     * @return the element to display
     */
    T getInfo(Coordinate where, boolean hit);

}
