package edu.duke.zw257.battleship.shipFactory.ship;

import edu.duke.zw257.battleship.coordinate.Coordinate;
import edu.duke.zw257.battleship.displayInfo.SimpleShipDisplayInfo;
import edu.duke.zw257.battleship.placement.Placement;

import java.util.HashMap;
import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
    protected final String name;

    /**
     * constructor of a rectangle ship
     * @param name name of this ship
     * @param pieces a map of coordinates and booleans
     * @param myDisplayInfo my displayInfo of this ship
     * @param enemyDisplayInfo enemy displayInfo of this ship
     */
    public RectangleShip(String name, Placement placement,
                         HashMap<Coordinate, Boolean> pieces,
                         SimpleShipDisplayInfo<T> myDisplayInfo,
                         SimpleShipDisplayInfo<T> enemyDisplayInfo) {
        super(placement, pieces, myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }


    /**
     * constructor of a rectangle ship
     * @param name name of this ship
     * @param placement the upperLeft coordination of this ship
     * @param width the width of this ship
     * @param height the height of this ship
     * @param myDisplayInfo my displayInfo of this ship
     * @param enemyDisplayInfo enemy displayInfo of this ship
     */
    public RectangleShip(String name, Placement placement, int width, int height, SimpleShipDisplayInfo<T> myDisplayInfo, SimpleShipDisplayInfo<T> enemyDisplayInfo) {
        super(placement, makeCoords(placement.getCoordinate(), width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * constructor of a rectangle ship
     * @param name name of this ship
     * @param placement the upperLeft coordination of this ship
     * @param width the width of this ship
     * @param height the height of this ship
     * @param data the data to display for this ship
     * @param onHit the data to display for this when hit
     */
    public RectangleShip(String name, Placement placement, int width, int height, T data, T onHit) {
        this(name, placement, width, height, new SimpleShipDisplayInfo<>(data, onHit), new SimpleShipDisplayInfo<>(null, data));
    }


    /**
     * easy for test, this should not be used
     * @param placement the upperLeft coordination of this ship
     * @param data the data to display for this ship
     * @param onHit the data to display for this when hit
     */
    public RectangleShip(Placement placement, T data, T onHit) {
        this("testship", placement, 1, 1, data, onHit);
    }

    /**
     * make coordinates for a ship
     * @param upperLeft the upperLeft coordinate of this ship
     * @param width the width of this ship
     * @param height the height of this ship
     * @return
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> result = new HashSet<>();
        if (width * height <= 0) return result;
        for (int w = 0; w < width; ++ w) {
            for (int h = 0; h < height; ++ h) {
                 result.add(new Coordinate(h + upperLeft.getRow(), w + upperLeft.getColumn()));
            }
        }
        return result;
    }

    /**
     * Get the name of this Ship, such as "submarine".
     * @return the name of this ship
     */
    @Override
    public String getName() {
        return this.name;
    }

}
