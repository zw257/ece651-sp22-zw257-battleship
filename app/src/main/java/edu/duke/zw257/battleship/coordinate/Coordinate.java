package edu.duke.zw257.battleship.coordinate;

public class Coordinate {
    private final int row, column;

    /**
     * Coordinate constructor
     * Constructs a Coordinate with specified row, and column.
     * @param row number of row
     * @param column number of column
     */

    public Coordinate (int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Coordinate constructor
     * @param descr descr.lenght() should be 2,
     *      the first char (should between "A" and "Z") indicates the number of row;
     *      the second char (should be a digit) indicates the number of column;
     */
    public Coordinate (String descr) {
        if (descr == null || descr.length() != 2) {
            throw new IllegalArgumentException("Invalid Coordinate Arguments Length (which should be 2)");
        }
        // row
        if (!Character.isAlphabetic(descr.charAt(0)) || Character.isDigit(descr.charAt(0))){
            throw new IllegalArgumentException("Invalid Coordinate Arguments");
        }
        this.row = Character.toUpperCase(descr.charAt(0)) - 'A';
        // column
        int columnValue;
        if (! Character.isDigit(descr.charAt(1))){
            throw new IllegalArgumentException("Invalid Coordinate Arguments");
        }
        columnValue = descr.charAt(1) - '0';
        this.column = columnValue;
    }


    @Override
    public boolean equals (Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    @Override
    public int hashCode () {
        return toString().hashCode();
    }

    @Override
    public String toString () {
        return "(" + row + ", " + column + ")";
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
}
