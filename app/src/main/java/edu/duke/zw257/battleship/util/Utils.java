package edu.duke.zw257.battleship.util;

import java.io.*;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static java.lang.Math.abs;

public class Utils {
    // 42313428
    public static Random rand = new Random(42313428);
    /**
     * random integer generator (from min to max - 1)
     * @param array, a char array
     * @return a random integer
     */
    public static char randomCharGenerator(List<Character> array) {
        return array.get(randomGenerator(array.size()));
    }

    /**
     * random integer generator (from min to max - 1)
     * @param array, a char array
     * @return a random integer
     */
    public static String randomStringGenerator(List<String> array) {
        int random = randomGenerator(array.size());
        return array.get(random);
    }


    /**
     * random integer generator
     * @param max the maximum of this number
     * @return a random integer
     */
    public static int randomGenerator(int max) {
        return randomGenerator(0, max);
    }

    /**
     * random integer generator (from min to max - 1)
     * @param min the maximum of this number
     * @param max the maximum of this number
     * @return a random integer
     */
    public static int randomGenerator(int min, int max) {
//        try {
//            sleep(100);
//        }catch (Exception ignored) {}
        if (max == min) return max;
        return (abs(rand.nextInt()) % (max-min)) + min;
    }

    /**
     * used to print a file in the resources directory
     * @param out is the where the output printStream
     * @param stream is the file name in the resources directory
     * @throws IOException if fail to read this stream
     */
    public static void printStream(PrintStream out, InputStream stream) throws IOException {
        if (out == null || stream == null || stream.available() == -1) {
            throw new IOException("Fail to read this stream and print.");
        }
        out.println(new String(stream.readAllBytes()).replace("\r", ""));
    }

    /**
     * read input stream from a bufferedReader and return a string which is containing in a required set
     * ignore cases
     * @param whiteList a set that the input should be one of it
     * @return string from input
     * @throws IOException if fail to read this stream
     * @throws EOFException while read an EOF or fail to read line
     */
    public static String readInputStrWithWhiteList(BufferedReader in, PrintStream out, Set<String> whiteList) throws IOException {
        if (in == null){
//            in = new BufferedReader(new InputStreamReader(System.in));
            return randomStringGenerator((List<String>) whiteList);
        }

        while (true) {
            String input = in.readLine();
            if (whiteList.contains(input)) {
                return input;
            }
            if (out != null) {
                out.println("Invalid input, please try again.");
            }
        }
    }

    /**
     * read input stream from a bufferedReader and return a String[]
     * ignore cases
     * @return string from input
     * @throws IOException if fail to read this stream
     * @throws EOFException while read an EOF or fail to read line
     */
    public static String[] readInputStrList(BufferedReader in, PrintStream out) throws IOException {
        if (in == null){
            in = new BufferedReader(new InputStreamReader(System.in));
        }
        String input = in.readLine();
        return input.split(" ");
    }



    /**
     * read input stream from a bufferedReader and return a string
     * @param inputReader a BufferedReader to read stream
     * @return string from input
     * @throws IOException if fail to read this stream
     * @throws EOFException while read a EOF
     */
    public static String readInputStr(BufferedReader inputReader) throws IOException {
        if (inputReader == null) {
            return readInputStr();
        }
        String input = inputReader.readLine();
        if (input == null) {
            throw new EOFException();
        }
        return input;
    }

    /**
     * read input stream from a bufferedReader and return a string
     * @return string from input
     * @throws IOException if fail to read this stream
     * @throws EOFException while read a EOF
     */
    public static String readInputStr() throws IOException {
        String input = new BufferedReader(new InputStreamReader(System.in)).readLine();
        if (input == null) {
            throw new EOFException();
        }
        return input;
    }

    /**
     * read input stream from a bufferedReader and return an int
     * @param inputReader a BufferedReader to read stream
     * @return int from input
     * @throws IOException if fail to read this stream
     * @throws EOFException while read a EOF
     * @throws NumberFormatException for fail to parse string to int
     */
    public static int readInputInt(BufferedReader inputReader, PrintStream out) throws IOException {
        String inputStr = readInputStr(inputReader);
        int result;
        try {
            result = Integer.parseInt(inputStr);
        } catch (NumberFormatException e) {
            if (out != null)
                out.print("\nPlease input an Integer: ");
            result = readInputInt(inputReader, out);
        }
        return result;
    }

    /**
     * read input stream from a bufferedReader and return an int
     *  [lower_boundary , upper_boundary] (accept equals)
     * @param inputReader a BufferedReader to read stream
     * @param lower_boundary return >= lower_boundary
     * @param upper_boundary return <= upper_boundary
     * @return int from input
     * @throws IOException if fail to read this stream
     * @throws EOFException while read a EOF
     * @throws NumberFormatException for fail to parse string to int
     */
    public static int readInputInt(BufferedReader inputReader, PrintStream out, int lower_boundary, int upper_boundary) throws IOException, NumberFormatException {
        if (inputReader == null) {
            return randomGenerator(lower_boundary, upper_boundary + 1);
        }
        int result;
        do  {
            if (out != null) out.print("Please input an Integer within ["+lower_boundary+", "+upper_boundary+"]: ");
            result = readInputInt(inputReader, null);
        } while (result < lower_boundary || upper_boundary < result);
        return result;
    }

    /**
     * read input stream from a bufferedReader and return an int
     * @param inputReader a BufferedReader to read stream
     * @return int from input
     * @throws IOException if fail to read this stream
     * @throws EOFException while read a EOF
     * @throws NumberFormatException for fail to parse string to int
     */
    public static Integer[] readInputIntList(BufferedReader inputReader, PrintStream out) throws IOException {
        String[] inputStrList = readInputStrList(inputReader, out);
        Integer[] result = new Integer[inputStrList.length];
        try {
            for (int i = 0; i < inputStrList.length; ++ i) {
                result[i] = Integer.parseInt(inputStrList[i]);
            }
            return result;
        } catch (NumberFormatException e) {
            if (out != null)
                out.println("Invalid input! please try again:");
            return readInputIntList(inputReader, out);
        }
    }


}
