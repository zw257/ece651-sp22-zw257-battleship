package edu.duke.zw257.battleship.placement;

import edu.duke.zw257.battleship.coordinate.Coordinate;

import java.util.HashSet;

public class Placement {
    private final Coordinate where;
    private final char orientation;


    /**
     * Placement constructor
     * @param where Coordinate
     * @param orientation a char for orientation
     */
    public Placement(Coordinate where, char orientation) {
        this.where = where;
        this.orientation = Character.toUpperCase(orientation);
    }

    /**
     * Placement constructor
     * @param descr  descr.lenght() should be 2,
     *      the first char (should between "A" and "Z") indicates the number of row;
     *      the second char (should be a digit) indicates the number of column;
     *      the first two chars would be passed into the Coordinate Class.
     *      the third char (should between "A" and "Z") indicates the orientation
     */
    public Placement(String descr) throws IllegalArgumentException{
        if (descr == null || descr.length() != 3) throw new IllegalArgumentException("Arguments length is not 3 in Placement Construction.");
        this.where = new Coordinate(descr.substring(0, 2));
        this.orientation = Character.toUpperCase(descr.charAt(2));
        // check arguments
        if (this.orientation < 'A' || this.orientation > 'z') throw new IllegalArgumentException("Invalid Arguments in Placement Construction.");
    }

    public Coordinate getCoordinate() {
        return where;
    }

    public char getOrientation() {
        return orientation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Placement p = (Placement) o;
        return orientation == p.orientation && where.equals(p.where);
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public String toString() {
        return "Placement{" +
                "where=" + where +
                ", orientation=" + orientation +
                '}';
    }
}
