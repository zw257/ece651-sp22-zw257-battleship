package edu.duke.zw257.battleship.board.checker;

import edu.duke.zw257.battleship.board.Board;
import edu.duke.zw257.battleship.shipFactory.ship.Ship;

public abstract class PlacementRuleChecker<T> {

    private final PlacementRuleChecker<T> next;
    // more stuff

    /**
     * the constructor of placement_rule_checker
     * @param next is the next chekcer in this chain
     */
    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    /**
     * the checking rule's detail
     * @param theShip is the ship to check
     * @param theBoard is the board to place
     * @return true if it can pass this checker
     */
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    /**
     * the maintaining the chain, if it pass, it would goto the next node of this chain
     * @param theShip is the ship to check
     * @param theBoard is the board to place
     * @return null for success, and return string for failure
     */
    public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        String checkingResult = checkMyRule(theShip, theBoard);
        if (checkingResult != null) {
            return checkingResult;
        }
        //otherwise, ask the rest of the chain.
        if (next != null) {
            return next.checkPlacement(theShip, theBoard);
        }
        //if there are no more rules, then the placement is legal
        return null;
    }

}
